# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig


def psd(signal, fs=1):
    f, pxx = sig.welch(signal)
    f = np.fft.fftshift(f) * fs
    pxx = np.fft.fftshift(pxx)
    
    plt.semilogy(f, pxx)
    plt.title('PSD')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Power / Hz [dB]')
    plt.grid()
    
    
def time_ap(signal, fs):
    dt = 1/fs
    t = np.arange(0, len(signal)) * dt
    
    plt.plot(t, np.abs(signal),  label='Amplitude')
    plt.plot(t, np.angle(signal),  label='Phase')
    plt.legend()
    plt.title('Amplitude & Phase')
    plt.xlabel('time [s]')
    plt.ylabel('Amplitude / Phase')
    plt.grid()
    
    
def time_iq(signal, fs):
    dt = 1/fs
    t = np.arange(0, len(signal)) * dt
    
    plt.plot(t, signal.real, label='real')
    plt.plot(t, signal.imag, label='imag')
    plt.legend()
    plt.title('Time Domain')
    plt.xlabel('time')
    plt.ylabel('Amplitude')
    plt.grid()


