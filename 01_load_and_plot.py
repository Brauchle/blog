# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

from functions import sigvis

# You'll have to add your filename here
filename = 'signals/gqrx_20191017_182750_107499000_1800000_fc.raw'
signal = np.fromfile(filename,np.complex64);

fs = 1800000


plt.figure()
sigvis.psd(signal, fs)


plt.figure()
sigvis.time_ap(signal[1:100], fs)


plt.figure()
sigvis.time_iq(signal[1:100], fs)